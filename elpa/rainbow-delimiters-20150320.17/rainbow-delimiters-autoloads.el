;;; rainbow-delimiters-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (rainbow-delimiters-mode-disable rainbow-delimiters-mode-enable
;;;;;;  rainbow-delimiters-mode) "rainbow-delimiters" "rainbow-delimiters.el"
;;;;;;  (22286 1409 744867 245000))
;;; Generated autoloads from rainbow-delimiters.el

(autoload 'rainbow-delimiters-mode "rainbow-delimiters" "\
Highlight nested parentheses, brackets, and braces according to their depth.

\(fn &optional ARG)" t nil)

(autoload 'rainbow-delimiters-mode-enable "rainbow-delimiters" "\
Enable `rainbow-delimiters-mode'.

\(fn)" nil nil)

(autoload 'rainbow-delimiters-mode-disable "rainbow-delimiters" "\
Disable `rainbow-delimiters-mode'.

\(fn)" nil nil)

;;;***

;;;### (autoloads nil nil ("rainbow-delimiters-pkg.el") (22286 1409
;;;;;;  855474 930000))

;;;***

(provide 'rainbow-delimiters-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; rainbow-delimiters-autoloads.el ends here
